# taz #

Contains Vagrantfile and settings for spinning up 2 x CoreOS VMs, core-01 and core-02.  After spinning up, 'vagrant ssh core-01' and 'cd coreos', then run start.sh, which will pull docker images for hokiett/nginx:azul and hokiett/railo:azul and run 1 container on each vm.  In the end, you'll be able to 'curl -L' to the IP of the one running nginx, which will have been notifiied of the railo container and serve it to you.

 