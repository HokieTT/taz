#! /bin/bash

rm -f ~/coreos/instances/*.service

while [[ $# > 0 ]] ; do
 ln -s ~/coreos/templates/railo@.service ~/coreos/instances/railo@$1.service
 echo "Railo service linked on port $1"
 shift
done

exit 0
