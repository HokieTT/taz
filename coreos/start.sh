#! /bin/bash

fleetctl submit ~/coreos/static/*
fleetctl load ~/coreos/static/*
fleetctl start ~/coreos/static/*

fleetctl submit ~/coreos/instances/*
fleetctl load ~/coreos/instances/*
fleetctl start ~/coreos/instances/*

fleetctl list-units

exit 0
